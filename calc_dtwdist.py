#!/usr/bin/env python
#-*- coding: utf-8 -*-

#import libraries
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import dircache as dc
import mlpy
import math
import sys
import copy
import os
import platform as pf

class DTWdistance:

    def __init__(self, src):
        self.readTxt(src)

    def readTxt(self, src):
        timepoints = []
        ratepoints = []
        f = open(src, 'r')
        for line in f:
            time, rate = line[: -1].split(' ')
            timepoints.append(float(time))
            ratepoints.append(float(rate))
        self.otime = timepoints
        self.orate = ratepoints        


def plot(src1, src2, output_path, obname1, obname2, dflag):

    s1 = copy.copy(src1)
    s2 = copy.copy(src2)
    s1s = [x for x in s1 if math.isnan(x) == False]
    s2s = [x for x in s2 if math.isnan(x) == False]

    splitchar = "\\" if pf.system() == 'Windows' else "/"
    plotfile = output_path + splitchar +  obname1 + "__" + obname2 + ".png"

    # slide the mate shape
    if dflag == 0:
    #   height
        maxsub = np.max(s2s) - np.max(s1s)
        if maxsub > 0:
            for i in range(len(s1)):
                if math.isnan(s1[i]) == False:
                    s1[i] += maxsub
        else:
            for i in range(len(s2)):
                if math.isnan(s2[i]) == False:
                    s2[i] += -maxsub
    length = max(len(s1), len(s2))

    plt.clf()
    plt.xlim([0, length])
    if dflag == 0:
        plt.ylim([-0.3, 1.0])
    else:
        plt.ylim([-0.2, 0.2])

    plt.plot(s1s, '--', color="#ffc0cb", linewidth=0.5)
    plt.plot(s2s, '--', color="#add8e6", linewidth=0.5)
    plt.plot(s1, 'r-', label=obname1)
    plt.plot(s2, 'b-', label=obname2)
    plt.legend(loc='upper right', fontsize=8)

    plt.suptitle("Comparing: " + obname1 + ", " + obname2)

    plt.savefig(plotfile)


if __name__=='__main__':

    if len(sys.argv) != 2:
        print("Usage: # python %s data-directory" % sys.argv[0])
        quit()

    splitchar = "\\" if pf.system() == 'Windows' else "/"

    input_path = sys.argv[1]
    sep_ip = input_path.split(splitchar)

    output_path = "DTWresult"
    if os.path.exists(output_path) == False:
        os.mkdir(output_path)

    objects = dc.listdir(input_path)

    fo1 = open(output_path + splitchar + sep_ip[-1] + ".csv", 'w')
    fo1.write("Object1,Object2,DTW-Distance\n")

    # if you want output [plot].png etc.
    if sys.argv[1][-4 :] == "DDTW":
        output_path = output_path + splitchar + "compares" + splitchar + "DDTW"
        dflag = 1
    else:
        output_path = output_path + splitchar + "compares" + splitchar + "DTW"
        dflag = 0
    if os.path.exists(output_path) == False:
        os.makedirs(output_path)

    # culcurate DTW distance in all combination of outbursts
    for x in range(len(objects)):

        fi1 = input_path + splitchar + objects[x]
        data1 = DTWdistance(fi1)

        for y in range(x + 1, len(objects)):

            fi2 = input_path + splitchar + objects[y]
            data2 = DTWdistance(fi2)

            # if you want only [dist],
            # dist = mlpy.dtw_std(data1.orate, data2.orate, dist_only = True)
            dist, cost, path = mlpy.dtw_std(data1.orate, data2.orate, dist_only = False)

            dist = dist / float(max(len(data1.otime), len(data2.otime)))

            # output result
            fo1.write("%s,%s,%f\n" % (objects[x][: -4], objects[y][: -4], dist))
            print("Compere [%s] and [%s]: %f\n" % (objects[x][: -4], objects[y][: -4], dist))
            
            # output path
            pathdir = output_path + splitchar + "path"
            if os.path.exists(pathdir) == False:
                os.mkdir(pathdir)
            pathfile = pathdir + splitchar + objects[x][: -4] + "__" + objects[y][: -4]
            fo2 = open(pathfile + ".csv", 'w')
            fo2.write("# 2 indexes in the same row are matched by DTW\n")
            # max value matching (width)
            if dflag == 0:
                maxidx1 = data1.orate.index(max(data1.orate))
                maxidx2 = data2.orate.index(max(data2.orate))
                if maxidx1 <= maxidx2:
                    slideobj = objects[x][: -4]
                    slidedays = maxidx2 - maxidx1
                    nanpad = [float("Nan")] * slidedays
                    nanpad.extend(data1.orate)
                    plotdata1 = nanpad
                    plotdata2 = data2.orate
                else:
                    slideobj = objects[y][: -4]
                    slidedays = maxidx1 - maxidx2
                    nanpad = [float("Nan")] * slidedays
                    nanpad.extend(data2.orate)
                    plotdata1 = data1.orate
                    plotdata2 = nanpad
            else:
                fi3 = open("DTWresult/compares/DTW/path/" + objects[x][: -4] + "__" + objects[y][: -4] + ".csv", 'r')
                for line in fi3:
                    if "width" in line:
                        slideobj = line.split(",")[1]
                        slidedays = int((line.split(","))[2])
                        print("%s, %d" % (slideobj, slidedays))
                fi3.close()
                nanpad = [float("Nan")] * slidedays
                if slideobj == objects[x][: -4]:
                    nanpad.extend(data1.orate)
                    plotdata1 = nanpad
                    plotdata2 = data2.orate
                else:
                    nanpad.extend(data2.orate)
                    plotdata1 = data1.orate
                    plotdata2 = nanpad                
            fo2.write("max value matching(width):,%s,%d\n" % (slideobj, slidedays))
            # maxvalue matching (height)
            if dflag == 0:
                maxsub = np.max(data2.orate) - np.max(data1.orate)
                if maxsub >= 0:
                    slideobj = objects[x][: -4]
                    sliderate = maxsub
                else:
                    slideobj = objects[y][: -4]
                    sliderate = -maxsub
                fo2.write("max value matching(height):,%s,%f\n" % (slideobj, sliderate))
            fo2.write("%s,%s\n" % (objects[x][: -4], objects[y][: -4]))
            for k in range(len(path[0])):
                fo2.write("%d,%d\n" % (path[0][k], path[1][k]))
            fo2.close()

            # plot normalized, smoothed rate (use if you want)
            ratedir = output_path + splitchar + "rate" 
            if os.path.exists(ratedir) == False:
                os.mkdir(ratedir)
            plot(plotdata1, plotdata2, ratedir, objects[x][: -4], objects[y][: -4], dflag)

            # plot DTW-path (use if you want)
            plt.clf()
            fig = plt.figure(1)
            ax = fig.add_subplot(111)
            plt.xlabel("index of [%s]" % objects[x][: -4])
            plt.ylabel("index of [%s]" % objects[y][: -4])
            plot1 = plt.imshow(cost.T, origin='lower', cmap=cm.gray, interpolation='nearest')
            plot2 = plt.plot(path[0], path[1], 'w')
            xlim = ax.set_xlim((-0.5, cost.shape[0]-0.5))
            ylim = ax.set_ylim((-0.5, cost.shape[1]-0.5))
            plt.savefig(pathfile + ".png")
            
    fo1.close()


