import numpy

def dtw(ts1, ts2, dist_only = False):
    table_d = numpy.zeros((len(ts1), len(ts2)))
    table_c = numpy.zeros((len(ts1), len(ts2)))

    # Make dist table
    for i in range(len(ts1)):
        for j in range(len(ts2)):
            table_d[i][j] = abs(ts1[i] - ts2[j])
            
    # Make cost table
    table_c[0][0] = table_d[0][0]

    for i in range(1, len(ts1)):
        table_c[i][0] = table_d[i][0] + table_c[i - 1][0]

    for j in range(1, len(ts2)):
        table_c[0][j] = table_d[0][j] + table_c[0][j - 1]
    
    for i in range(1, len(ts1)):
        for j in range(1, len(ts2)):
            table_c[i][j] = table_d[i][j] + min(min(table_c[i][j-1], table_c[i-1][j]), table_c[i-1][j-1])

    # Return values
    if dist_only:
        return table_c[-1][-1]
    else:
        path = ([len(ts1) - 1], [len(ts2) - 1])

        i, j = len(ts1) - 1, len(ts2) - 1

        while i > 0 or j > 0:
            
            if i == 0:
                j -= 1
            elif j == 0:
                i -= 1
            else:
                m = min(min(table_c[i][j - 1], table_c[i - 1][j]), table_c[i - 1][j - 1])
                
                if   m == table_c[i - 1][j]:
                    i -= 1
                elif m == table_c[i][j - 1]:
                    j -= 1
                else:
                    i -= 1
                    j -= 1

            path[0].append(i)
            path[1].append(j)
            

        path = (numpy.array(list(reversed(path[0]))), numpy.array(list(reversed(path[1]))))
        return table_c[-1][-1], table_c, path
