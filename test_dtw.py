# -*- coding:utf-8 -*-

import unittest2 as unittest

import dtw
import mlpy

import numpy

REPEAT = 10
LENGTH = 100


class DTWTestCase(unittest.TestCase):

    
    def setUp(self):
        self.mlpy = []
        self.mydtw = []
        
        for i in range(REPEAT):
            x = numpy.linspace(-pow(100 + i, 2), pow(100 + i, 2), LENGTH)
            y = [numpy.sin(xx) + numpy.cos(xx + i) for xx in x] * 2

            self.mlpy.append( mlpy.dtw_std(x, y, dist_only = False) )
            self.mydtw.append( dtw.dtw(x, y, dist_only = False) )

        
    def test_dist(self):
        sum_mlpy  = sum([x[0] for x in self.mlpy])
        sum_mydtw = sum([x[0] for x in self.mydtw])
        self.assertAlmostEqual(sum_mlpy, sum_mydtw, places = 4, msg = "dist_mlpy =~ dist_mydtw")

        
    def test_cost(self):
        for i in range(REPEAT):
            cost_mlpy  = self.mlpy[i][1]
            cost_mydtw = self.mydtw[i][1]

            self.assertEqual(type(cost_mlpy), type(cost_mydtw), msg = "same type (numpy.ndarray)")
            self.assertEqual(cost_mlpy.shape, cost_mydtw.shape, msg = "same shape (len(x) * len(y))")
            self.assertTrue(numpy.array_equal(cost_mlpy, cost_mydtw), msg = "equal")

            
    def test_path(self):

        for i in range(REPEAT):
            path_mlpy  = self.mlpy[i][2]
            path_mydtw = self.mydtw[i][2]
            
            self.assertEqual(type(path_mlpy), type(path_mydtw), msg = "type is tuple(List, List)")
            self.assertEqual(len(path_mlpy[0]), len(path_mydtw[0]), msg = "len(x[0]) == len(x[0])")
            self.assertEqual(len(path_mlpy[1]), len(path_mydtw[1]), msg = "len(x[1]) == len(x[1])")

            self.assertTrue(numpy.array_equal(path_mlpy[0], path_mydtw[0]), "elements equal 0")
            self.assertTrue(numpy.array_equal(path_mlpy[1], path_mydtw[1]), "elements equal 1")


    def test_dimensions(self):
        x = [0] * 100
        y = [0]

        dtw_mlpy  = mlpy.dtw_std(x, y, dist_only = True)
        dtw_mydtw = dtw.dtw(x, y, dist_only = True)
        self.assertEqual(dtw_mlpy, dtw_mydtw, "dimension 100 x 1 ok")

        dtw_mlpy  = mlpy.dtw_std(y, x, dist_only = True)
        dtw_mydtw = dtw.dtw(y, x, dist_only = True)
        self.assertEqual(dtw_mlpy, dtw_mydtw, "dimension 1 x 100 ok")

        dtw_mlpy  = mlpy.dtw_std(y, y, dist_only = True)
        dtw_mydtw = dtw.dtw(y, y, dist_only = True)
        self.assertEqual(dtw_mlpy, dtw_mydtw, "dimension 1 x 1 ok")

            
        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(DTWTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
    
